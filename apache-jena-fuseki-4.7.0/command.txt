R.-S. Shaw, C.-F. Tsao, and P.-W. Wu, “A study of the application of ontology to an FAQ automatic classification system,” Expert Syst Appl, vol. 39, no. 14, pp. 11593–11606, Oct. 2012, doi: 10.1016/j.eswa.2012.04.022.
M. Karan and J. Šnajder, “Paraphrase-focused learning to rank for domain-specific frequently asked questions retrieval,” Expert Syst Appl, vol. 91, pp. 418–433, Jan. 2018, doi: 10.1016/j.eswa.2017.09.031.
Google, “Introducing the KG: Things, not strings.,” https://blog.google/products/search/introducing-knowledge-graph-things-not/, May 06, 2012.
L. Li et al., “A knowledge graph completion model based on contrastive learning and relation enhancement method,” Knowl Based Syst, vol. 256, p. 109889, Nov. 2022, doi: 10.1016/j.knosys.2022.109889.
W. Min, C. Liu, L. Xu, and S. Jiang, “Applications of knowledge graphs for food science and industry,” Patterns, vol. 3, no. 5, p. 100484, May 2022, doi: 10.1016/j.patter.2022.100484.
X. Chen, S. Jia, and Y. Xiang, “A review: Knowledge reasoning over knowledge graph,” Expert Syst Appl, vol. 141, p. 112948, Mar. 2020, doi: 10.1016/j.eswa.2019.112948.
W. Ali, M. Saleem, B. Yao, A. Hogan, and A.-C. N. Ngomo, “A survey of RDF stores & SPARQL engines for querying knowledge graphs,” The VLDB Journal, vol. 31, no. 3, pp. 1–26, May 2022, doi: 10.1007/s00778-021-00711-3.
World Wide Web Consortium (W3C), “SKOS simple knowledge organization system reference,” https://www.w3.org/TR/2009/REC-skos-reference- 20090818/#L895, Aug. 18, 2009.
R. Al-Rfou, B. Perozzi, and S. Skiena, “Polyglot: Distributed Word Representations for Multilingual NLP,” Jul. 2013.
A. Dinakaramani, F. Rashel, A. Luthfi, and R. Manurung, “Designing an Indonesian part of speech tagset and manually tagged Indonesian corpus,” in 2014 International Conference on Asian Language Processing (IALP), IEEE, Oct. 2014, pp. 66–69. doi: 10.1109/IALP.2014.6973519.
H. Ishkewy, H. Harb, and H. Farahat, “Azhary: An Arabic Lexical Ontology,” International journal of Web & Semantic Technology, vol. 5, no. 4, pp. 71–82, Oct. 2014, doi: 10.5121/ijwest.2014.5405.
A. Narang, R. K. Sharma, and P. Kumar, “Development of Punjabi WordNet,” CSI Transactions on ICT, vol. 1, no. 4, pp. 349–354, Dec. 2013, doi: 10.1007/s40012-013-0034-0.
C. Fellbaum, WordNet: An Electronic Lexical Database. The MIT Press, 1998. doi: 10.7551/mitpress/7287.001.0001.
S. Han and C. K. Anderson, “Web Scraping for Hospitality Research: Overview, Opportunities, and Implications,” Cornell Hospitality Quarterly, vol. 62, no. 1, pp. 89–104, Feb. 2021, doi: 10.1177/1938965520973587.
World Wide Web Consortium (W3C), “RDF 1.1 turtle,” https://www.w3.org/TR/turtle/, Feb. 25, 2014.
G. A. Miller and C. Fellbaum, “Semantic networks of english,” Cognition, vol. 41, no. 1–3, pp. 197–229, Dec. 1991, doi: 10.1016/0010-0277(91)90036-4.
Gunawan and E. Pranata, “Acquisition of Hypernymy-Hyponymy Relation between Nouns for WordNet Building,” in 2010 International Conference on Asian Language Processing, IEEE, Dec. 2010, pp. 114–117. doi: 10.1109/IALP.2010.70.
 
