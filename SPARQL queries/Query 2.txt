#Find all FAQs that overlap the buyer "Pembeli" and seller "Penjual" sides

PREFIX ex: <http://example.org/> 
PREFIX ex-verb: <http://example.org/verb/> 
PREFIX ex-noun: <http://example.org/noun/> 
PREFIX ex-adj: <http://example.org/adj/> 
PREFIX ex-class: <http://example.org/class/> 
PREFIX ex-faq: <http://example.org/faq/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
PREFIX dct: <http://purl.org/dc/terms/>


SELECT DISTINCT ?faqTitle
WHERE {
  ?faq rdf:type ex-class:faq .
  ?faq dct:title ?faqTitle .
  ?conceptPenjual rdf:type skos:Concept ;
           skos:prefLabel "Penjual" .
  ?conceptPembeli rdf:type skos:Concept ;
           skos:prefLabel "Pembeli" . 
  ?faq dct:subject/skos:broader* ?conceptPembeli ;
  		dct:subject/skos:broader* ?conceptPenjual .
}
